package org.oop;

public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getDistanceFrom(Point p1) {
        double xDistance = x - p1.x;
        double yDistance = y - p1.y;
        return Math.sqrt(Math.pow(xDistance,2)+Math.pow(yDistance,2));
    }

    public double getDirectionTo(Point p1) {
        double xDistance =p1.x - x;
        double yDistance =p1.y - y;
        return Math.atan2(yDistance, xDistance);
    }
}
